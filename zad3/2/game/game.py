import math
import time
import random
from utils.utils import get_question


def game(difficulty, time_limitation):
    base_time_limit = 3
    time_limit = 3.5
    offset = 1
    scale = 2
    base = 0.9
    level = 1
    score = 0
    chances = 3
    multiplier = 1
    bonus_counter = 0

    while chances > 0:
        print("Pozostale szanse: ", chances)
        print("Aktualny wynik: ", score)
        luck = random.randint(0, 50)
        if luck == 0:
            print("Masz szczescie, otrzymujesz dodatkowa szanse!\nBy kontynuowac nacisnij enter...")
            chances += 1
            input('')
        elif luck == 1:
            print("Masz szczescie, przez kolejne trzy rundy otrzymujesz podwojna ilosc punktow!\nBy kontynuowac nacisnij enter...")
            multiplier = 2
            bonus_counter = 3
            input('')
        else:
            expression = get_question(level)
            print(expression.body, '=', end=' ')
            start_time = time.time()
            x = input('').upper()
            answer_time = time.time()

            if x == expression.value and answer_time - start_time < time_limit:
                print("Dobra odpowiedz\n")
                if multiplier > 1 and bonus_counter > 0:
                    score += level * multiplier
                    bonus_counter -= 1
                else:
                    score += level

                if time_limitation == '2':
                    time_limit = scale * base ** score + offset
                elif time_limitation == '1':
                    time_limit = base_time_limit + int(math.sqrt(score))

                if difficulty == '1':
                    level = int(math.log(score + 2, 3))

            elif x == expression.value and not (answer_time - start_time < time_limit):
                print("Za wolno!\n")
                chances -= 1
            else:
                print("Zla odpowiedz!\n")
                chances -= 1

    bonus = 0
    if time_limitation == '2':
        bonus += score
    elif time_limitation == '1':
        bonus -= score * 0.2
    return score + bonus


def get_description():
    return {
        'rules': "Gra polega na okresleniu wartosci podanego wyrazenia logicznego w odpowiednim czasie.\nPrzykladowo wyrazenie moze zostac wyswietlone w postaci '1 and x = '. Twoim zadaniem jest wpisanie odpowiedniej wartosci(1, 0 lub X) oraz potwierdzenie wyboru klawiszem enter. Wielkosc wpisanej litery X nie ma znaczenia. Podczas rozgrywki masz do wykorzystania trzy szanse.\nNacisnij enter aby kontynuowac...",
        'difficulties': "\t0 - wyswietlane wyrazenia logiczne zawieraja zawsze tylko jeden operator\n\t1 - z czasem wyrazenia logiczne sa coraz bardziej zlozone, ale daja wiecej punktow\n\tTwoj wybor(domyslnie 0): ",
        'time_limitation': "\t0 - na odpowiedz masz 3.5 sekundy\n\t1 - na poczatku masz 3 sekundy na odpowiedz, z biegiem gry masz go coraz wiecej(-20% punktow)\n\t2 - na poczatku masz 3.5 sekundy, z biegiem gry masz coraz mniej czasu na odpowiedz(+100% punktow)\n\tTwoj wybor(domyslnie 0):"
    }
