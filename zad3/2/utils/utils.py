import getopt
import random
import sys
import pandas as pd


def _get_operator(file_name):
    if file_name != '':
        return pd.read_csv(file_name, sep=' ', header=None, dtype=str).values
    else:
        return []


def get_operators():
    notfile = ''
    andfile = ''
    orfile = ''
    impfile = ''

    try:
        opts, _ = getopt.getopt(sys.argv[1:], '', ['not=', 'and=', 'or=', 'imp='])
    except getopt.GetoptError:
        print("Wrong arguments")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--not':
            notfile = arg
        elif opt == '--and':
            andfile = arg
        elif opt == '--or':
            orfile = arg
        elif opt == '--imp':
            impfile = arg

    return {
        'not': _get_operator(notfile),
        'and': _get_operator(andfile),
        'or': _get_operator(orfile),
        'imp': _get_operator(impfile)
    }


OPERATORS = get_operators()


def no_operators():
    no_ops = True
    for value in OPERATORS.values():
        if value != []:
            no_ops = False

    return no_ops


def _get_complex_expression(exp):
    operator_table = []
    while operator_table == []:
        operator_name, operator_table = random.choice(list(OPERATORS.items()))
    operator_table = filter(lambda x: x[0] == exp.value, operator_table)
    values = random.choice(tuple(operator_table))
    if operator_name == 'not':
        exp.body = operator_name + '(' + exp.body + ')'
    else:
        if operator_name == 'imp' or random.randint(0, 1) == 0:
            exp.body = '(' + exp.body + ') ' + operator_name + ' ' + values[1]
        else:
            exp.body = values[1] + ' ' + operator_name + ' (' + exp.body + ') '
    exp.value = values[-1]
    return exp


def get_question(n):
    operator_table = []
    while operator_table == []:
        operator_name, operator_table = random.choice(list(OPERATORS.items()))
    line = random.choice(operator_table)
    exp = Expression(operator_name, line)
    while n > 1:
        exp = _get_complex_expression(exp)
        n -= 1

    return exp


class Expression:
    def __init__(self, operator_name, values):
        if operator_name == 'not':
            self.body = ' '.join((operator_name, values[0]))
        else:
            self.body = ' '.join((values[0], operator_name, values[1]))

        self.value = values[-1]
