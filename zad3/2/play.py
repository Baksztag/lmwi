from game.game import game, get_description
from utils.utils import no_operators


def play():
    if no_operators():
        print("Aby uruchomic gre konieczne jest wczytanie definicji operatorow logicznych z linii polecen.")
        return 1

    game_description = get_description()
    print(game_description['rules'])
    input('')

    while True:
        print("Wybierz poziom trudnosci rozgrywki:")
        difficulty = input(game_description['difficulties'])

        print("Wybierz ile czasu bedziesz miec na odpowiedz:")
        time_limitation = input(game_description['time_limitation'])

        input('Nacisnij enter aby rozpoczac gre...')
        result = game(difficulty, time_limitation)
        print('Koniec gry.\nTwoj wynik: ', result)

        if input('Czy chcesz grac dalej? [Y/n]') == 'n':
            break

if __name__ == '__main__':
    play()


