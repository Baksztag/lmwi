import sys


def main(argv):
    if len(argv) != 6:
        exit('Niepoprawna liczba parametrow (wymagane 6)')

    p = []
    for parameter in argv:
        p.append(int(parameter))

    if (p[0] >= p[2] and (p[0] != p[2] or p[1] > p[3])) \
            or (p[2] >= p[4] and (p[2] != p[4] or p[3] > p[5])):
        print('0')
    else:
        print('1')


if __name__ == '__main__':
    main(sys.argv[1:])